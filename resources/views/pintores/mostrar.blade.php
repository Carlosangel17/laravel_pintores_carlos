@extends("layouts.master")

@section("titulo")
	Datos pintor
@endsection
@section("contenido")

	<div class="row">
		
		<div class="col-xs-12 col-sm-12">
			<h3>{{$pintor->nombre}}</h3>
			<h4>Pais: {{$pintor->pais}}</h4>	
			<h4>Cuadros</h4>
		</div>
		@foreach( $cuadros as $cuadro)
			<div class="col-xs-12 col-sm-6 col-md-4 cajas_mascota" >
				<h6>{{$cuadro->nombre}}</h6>
				<img src="{{ asset('assets/images') }}/{{ $cuadro->imagen}}" class="img-fluid" style="height:200px"/>
				
			</div>
		@endforeach
	</div>
@endsection