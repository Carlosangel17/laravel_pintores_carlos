@extends("layouts.master")


@section("titulo")
	Pintores
@endsection
@section("contenido")

<div class="row">

	<table id="tablaPintores">
		<tr>
			<th>Nombre</th>
			<th>Pais</th>
			<th>Cuadros</th>
		</tr>
		@foreach( $pintores as $pintor)
			<tr>
				<td>
					<a href="{{ url('/pintores/mostrar/' . $pintor->id) }}">{{$pintor->nombre}}</a>
					
				</td>
				<td>
					{{$pintor->pais}}
				</td>
				

				
			</tr>

		@endforeach
	</table>
	
</div>
@endsection