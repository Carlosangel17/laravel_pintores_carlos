@extends("layouts.master")

@section("titulo")
Crear cuadro
@endsection
@section("contenido")
<div class="row">
	<div class="offset-md-3 col-md-6">
		<div class="card">
			<div class="card-header text-center">Añadir un nuevo cuadro</div>
			<div class="card-body" style="padding:30px">
				<form action="{{ url('cuadros/crear') }}" method="POST" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="form-group">
						<label for="campo_nombre">Nombre del cuadro</label>
						<input type="text" name="campo_nombre" id="campo_nombre" class="form-control">
					</div>
					<div class="form-group">
						<label for="campo_pintor">Pintor</label>
						<select name="campo_pintor">
							@foreach($pintores as $pintor)
								<option value="{{$pintor->id}}">
									{{$pintor->nombre}}
								</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="imagen">Imagen</label>
						<input type="file" name="imagen" id="imagen" class="form-control">
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Añadir cuadro</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection