<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pintor extends Model
{
    Protected $table = "pintores";

    public function getCuadros(){
    	return $this->hasMany("App\Cuadro");
    }

    
}
