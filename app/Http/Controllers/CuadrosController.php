<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pintor;
use App\Cuadro;
use Illuminate\Support\Facades\Storage;

class CuadrosController extends Controller
{
    public function getCrear(){
    	$pintores = Pintor::all();
    	return view("cuadros.crear", array("pintores" =>$pintores));
    }

    public function postCrear(Request $request){
    	$cuadro = new Cuadro();
    	$cuadro->nombre = $request->campo_nombre;
    	$cuadro->pintor_id = $request->campo_pintor;
    	$cuadro->imagen = $request->imagen->store("", "cuadros");
    	$cuadro->save();
    	return redirect("pintores/mostrar/" . $request->campo_pintor);
    	
    }
}
