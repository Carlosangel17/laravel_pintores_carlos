<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pintor;
use App\Cuadro;
use Illuminate\Support\Facades\Storage;

class PintoresController extends Controller
{

	public function getInicio(){
		return redirect('pintores');
	}
    public function getPintores(){
    	$pintores = Pintor::all();
    	return view("pintores.index", array("pintores" =>$pintores));
    }

    public function getMostrar($id){
    	$pintor = Pintor::findOrFail($id);
        $cuadros = $pintor->getCuadros;
    	return view("pintores.mostrar", array("pintor" => $pintor,"cuadros" =>$cuadros));
    }
}
